**Used:**

* LibGDX game framework 1.6.2

* Box2D

* Box2D lights 1.4.

*In this example you can see basic work with Box2D, LibGDX and Box2D lights.*

**Controls:** 

* W -- move right.

* S -- move left.

* A -- enable/disable left light.

* D -- enable/disable right light.

* Q -- enable/disable sun light.

* E|R -- move sun.

* F -- enable/disable debug renderer.